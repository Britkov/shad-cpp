cmake_minimum_required(VERSION 2.8)
project(spin-lock)

if (TEST_SOLUTION)
  include_directories(../private/spin-lock/)
endif()

include(../common.cmake)

add_gtest(test_spin_lock test.cpp)
add_benchmark(bench_spin_lock bench.cpp)
