cmake_minimum_required(VERSION 2.8)
project(stack)

if (TEST_SOLUTION)
    include_directories(../private/lock-free-stack/hazard-ptr)
endif()

include(../common.cmake)

add_gtest(test_stack test.cpp)

add_benchmark(bench_stack run.cpp)
